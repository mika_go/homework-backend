from django.contrib import admin
from .models import User, Concert


# Register your models here.
@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    fields = ("username", "password", "surname", "name", "patronymic")
    list_display = ("id", "username", "password", "surname", "name", "patronymic", "concerts_display")
    search_fields = ["surname", "name"]

    def concerts_display(self, obj):
        return ", ".join([
            concert.title for concert in Concert.objects.all().filter(users=obj.id)
        ])

    concerts_display.short_description = "концерты"


# Register your models here.
@admin.register(Concert)
class ConcertAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "date", "cost", "users_display")
    date_hierarchy = "date"
    search_fields = ["title"]

    def users_display(self, obj):
        return ", ".join([
            "%s %s %s" % (user.surname, user.name, user.patronymic) for user in obj.users.all()
        ])

    users_display.short_description = "пользователи"
