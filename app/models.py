from django.db import models


# Create your models here.
class User(models.Model):
    id = models.AutoField(primary_key=True)
    username = models.CharField(verbose_name="логин", max_length=50)
    password = models.CharField(verbose_name="пароль", max_length=50)
    name = models.CharField(verbose_name="имя", max_length=80)
    surname = models.CharField(verbose_name="фамилия", max_length=80)
    patronymic = models.CharField(verbose_name="отчество", max_length=80)

    class Meta:
        ordering = ["surname"]
        verbose_name = "пользователь"
        verbose_name_plural = "пользователи"

    def __str__(self):
        return "%s %s %s" % (self.surname, self.name, self.patronymic)


class Concert(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(verbose_name="название", max_length=80)
    date = models.DateTimeField(verbose_name="дата проведения")
    cost = models.FloatField(verbose_name="цена билета")
    users = models.ManyToManyField(User, verbose_name="пользователи")

    class Meta:
        ordering = ["date"]
        verbose_name = "концерт"
        verbose_name_plural = "концерты"

    def __str__(self):
        return self.title
