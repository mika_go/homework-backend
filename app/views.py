import json
import math

from django.http import HttpResponse, HttpRequest, JsonResponse, HttpResponseBadRequest, HttpResponseNotFound, HttpResponseNotAllowed
from django.views import View
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt

from app.models import User, Concert


@csrf_exempt
def auth(req: HttpRequest):
    if req.session.get("is_logged"):
        return HttpResponse()
    else:
        return HttpResponseNotFound()


@csrf_exempt
def login(req: HttpRequest):
    try:
        data = json.loads(req.body.decode())
        try:
            u = User.objects.get(username=data["username"])
        except User.DoesNotExist:
            u = None
        if not u:
            return HttpResponseNotFound()
        if u.password == data["password"]:
            req.session["is_logged"] = True
            return HttpResponse()
        else:
            return HttpResponseNotFound()
    except KeyError:
        return HttpResponseBadRequest()


@csrf_exempt
def register(req: HttpRequest):
    data = json.loads(req.body.decode())
    try:
        try:
            u = User.objects.get(username=data["username"])
        except User.DoesNotExist:
            u = None
        if u:
            return HttpResponseNotFound()
        else:
            u = User(username=data["username"], password=data["password"], name=data["name"],
                     surname=data["surname"], patronymic=data["patronymic"])
            u.save()
            req.session["is_logged"] = True
            return JsonResponse({"id": u.id})
    except KeyError:
        return HttpResponseBadRequest()


@csrf_exempt
def logout(req: HttpRequest):
    try:
        del req.session["is_logged"]
    except KeyError:
        pass
    return HttpResponse()


@csrf_exempt
def users_pages(req: HttpRequest):
    if not req.session.get("is_logged"):
        return HttpResponseNotAllowed
    return JsonResponse({"pages": math.ceil(User.objects.count() / 5)})


@csrf_exempt
def concerts_pages(req: HttpRequest):
    if not req.session.get("is_logged"):
        return HttpResponseNotAllowed
    return JsonResponse({"pages": math.ceil(Concert.objects.count() / 5)})


class UsersView(View):
    batch_size = 5

    @csrf_exempt
    def get(self, req: HttpRequest):
        if not req.session.get("is_logged"):
            return HttpResponseNotAllowed
        if req.GET.get("id"):
            user = User.objects.get(id=req.GET["id"])
            json = serializers.serialize("json", [user])
            if user:
                return HttpResponse(json, content_type="application/json")
            else:
                return HttpResponseNotFound()
        else:
            if req.GET.get("page"):
                page = int(req.GET["page"])
                users = User.objects.all().order_by("id")[page * self.batch_size:(page + 1) * self.batch_size]
            else:
                users = User.objects.all().order_by("id")[:self.batch_size]

            json = serializers.serialize("json", users)
            return HttpResponse(json, content_type="application/json")

    @csrf_exempt
    def post(self, req: HttpRequest):
        if not req.session.get("is_logged"):
            return HttpResponseNotAllowed
        data = json.loads(req.body.decode())
        try:
            try:
                u = User.objects.get(username=data["username"])
            except User.DoesNotExist:
                u = None
            if u:
                return HttpResponseNotFound()
            else:
                u = User(username=data["username"], password=data["password"], name=data["name"],
                         surname=data["surname"], patronymic=data["patronymic"])
                u.save()
                return JsonResponse({"id": u.id})
        except KeyError:
            return HttpResponseBadRequest()

    @csrf_exempt
    def put(self, req: HttpRequest):
        if not req.session.get("is_logged"):
            return HttpResponseNotAllowed
        if req.GET.get("id"):
            user = User.objects.get(id=req.GET["id"])
            data = json.loads(req.body.decode())
            if data.get("username"):
                try:
                    u = User.objects.get(name=data["username"])
                except:
                    u = None
                if u:
                    return HttpResponseBadRequest()
                user.username = data["username"]
            if data.get("password"):
                user.password = data["password"]
            if data.get("name"):
                user.name = data["name"]
            if data.get("surname"):
                user.surname = data["surname"]
            if data.get("patronymic"):
                user.patronymic = data["patronymic"]

            user.save()
            return HttpResponse()

    @csrf_exempt
    def delete(self, req: HttpRequest):
        if not req.session.get("is_logged"):
            return HttpResponseNotAllowed
        if req.GET.get("id"):
            user = User.objects.get(id=req.GET["id"])
            if user:
                user.delete()
                return HttpResponse()
            else:
                return HttpResponseNotFound()


class ConcertsView(View):
    batch_size = 5

    @csrf_exempt
    def get(self, req: HttpRequest):
        if not req.session.get("is_logged"):
            return HttpResponseNotAllowed
        if req.GET.get("id"):
            concert = Concert.objects.get(id=req.GET["id"])
            json = serializers.serialize("json", [concert])
            if concert:
                return HttpResponse(json, content_type="application/json")
            else:
                return HttpResponseNotFound()
        else:
            if req.GET.get("page"):
                page = int(req.GET["page"])
                concerts = Concert.objects.all().order_by("id")[page * self.batch_size:(page + 1) * self.batch_size]
            else:
                concerts = Concert.objects.all().order_by("id")[:self.batch_size]

            json = serializers.serialize("json", concerts)
            return HttpResponse(json, content_type="application/json")

    @csrf_exempt
    def post(self, req: HttpRequest):
        if not req.session.get("is_logged"):
            return HttpResponseNotAllowed
        data = json.loads(req.body.decode())
        try:
            concert = Concert(title=data["title"], date=data["date"], cost=data["cost"])
            concert.save()
            concert.users.set(data["users"])
            concert.save()
            return JsonResponse({"id": concert.id})
        except KeyError:
            return HttpResponseBadRequest()

    @csrf_exempt
    def put(self, req: HttpRequest):
        if not req.session.get("is_logged"):
            return HttpResponseNotAllowed
        if req.GET.get("id"):
            concert = Concert.objects.get(id=req.GET["id"])
            data = json.loads(req.body.decode())
            if data.get("title"):
                concert.title = data["title"]
            if data.get("date"):
                concert.date = data["date"]
            if data.get("cost"):
                concert.cost = data["cost"]
            if data.get("users"):
                concert.users.set(data["users"])

            concert.save()
            return HttpResponse()

    @csrf_exempt
    def delete(self, req: HttpRequest):
        if not req.session.get("is_logged"):
            return HttpResponseNotAllowed
        if req.GET.get("id"):
            concert = Concert.objects.get(id=req.GET["id"])
            if concert:
                concert.delete()
                return HttpResponse()
            else:
                return HttpResponseNotFound()
