"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from app.views import auth, login, logout, register, UsersView, ConcertsView, users_pages, concerts_pages

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/users', UsersView.as_view()),
    path('api/concerts', ConcertsView.as_view()),
    path('api/auth', auth),
    path('api/login', login),
    path('api/register', register),
    path('api/logout', logout),
    path('api/pages/users', users_pages),
    path('api/pages/concerts', concerts_pages),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
